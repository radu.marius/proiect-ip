import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Tester {
    /* ... */
    public void printMovies(HttpRequest Request) throws Exception {
        StringBuffer response = Request.sendRequest();

        //Citim raspunsul JSON si il afisam
        JsonObject object = Json.parse(response).asObject();
        /* ... */

        String Title = object.getString("Title", "Nope");
    }