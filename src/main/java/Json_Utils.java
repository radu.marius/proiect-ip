import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/*** Class used for interpretign the json objects from URL requets  ***/

public class Json_Utils {
    private final String apiAddress = "http://www.omdbapi.com";
    private final String apiKey = "apikey=204dbe52";

    /***  Print the Json object's keys and values ***/
    void Provide_Info(String JsonString){
        JsonObject object = Json.parse(JsonString).asObject();
        String title = object.getString("Title", "Not Found");
        System.out.println("Movie title: " + title);
        String releaseDate = object.getString("Released", "Not Found");
        System.out.println("An aparitie: " + releaseDate);
        String actors = object.getString("Actors", "Not Found");
        System.out.println("Cast: " + actors);
        String Gen = object.getString("Genre", "Not Found");
        System.out.println("Gen: " + Gen);
        String Awards = object.getString("Awards", "Not Found");
        System.out.println("Premiii: " + Awards);
    }

    void PrintMovieDetailsById(String Id) {
        String JsonString = test("i=", "", Id);
        Provide_Info(JsonString);
    }

    void PrintMovieDetailsByTitle(String Title) {
        String JsonString = test("t=", Title, "");
        Provide_Info(JsonString);
    }

    void PrintMovieSummary(String Movie_Id){
        String JsonString = test("i=","",Movie_Id);
        JsonObject object = Json.parse(JsonString).asObject();
        System.out.println(object.getString("Plot","No Plot Available"));
    }


    void PrintMovie(String Movie_Id){
       String JsonString = test("i=","",Movie_Id);
       JsonObject object = Json.parse(JsonString).asObject();
       String title = object.getString("Title", "Not Found");
       System.out.println("Movie title: "+title);
    }

    /*** Search the movie by keyword and printign keys and values***/

    void SearchMovieByKeyWord(String KeyWord){
        String JsonString = test("s=",KeyWord,"");
        JsonObject object = Json.parse(JsonString).asObject();
        JsonArray Movies = object.get("Search").asArray();
        int max_movies = 5; /*** max 5 movies to list by keyword       ***/
        for (JsonValue item : Movies) {
            String Title = item.asObject().getString("Title", "NOT Found");
            String Year = item.asObject().getString("Year", "NOT Found");
            String imdbID = item.asObject().getString("imdbID", "NOT Found");
            String Type = item.asObject().getString("Type", "NOT Found");
            System.out.println("Title: "+Title+"Type: "+Type+"Year: "+Year+"imdb ID: "+imdbID);
            max_movies--;
            if(max_movies == 0)
                break;
        }
    }

    /*** Url Get request . MethodOfView used for :  "=s, =t and =i *
     *   return a String having Json content
     ***/
    public String test(String MethodOfView, String Title, String Id) {

        StringBuilder content = new StringBuilder();

        // link-ul finale către care vom face cererea. Contine parametrii GET necesari pentru cererea noastră.
        String requestString = apiAddress + "?" + MethodOfView + Title + Id + "&" + apiKey;
        System.out.println("\n\n request String : " + apiAddress + "?" + MethodOfView + Title + Id + "&" + apiKey + "\n\n\n");

        try {
            // creăm un obiec URL pe baza request-string-ului
            URL url = new URL(requestString);

            // Deschidem conexiunea
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            // setăm metoda HTTP
            con.setRequestMethod("GET");

            // trimitem cererea si asteptam raspunsul. status va conține codul intors de cererea noastră(ex. 200).
            int status = con.getResponseCode();

            // Creăm un BufferedReader pentru a citi informația de la linkul respectiv
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));

            String inputLine;

            // Citim toate liniile si le appendu-im in content

            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }

            // content va contine practic datele in format json. Il vom trimite catre parser-ul de json ulterior.

            // Inchidem reader-ul si conexiunea
            in.close();
            con.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return content.toString();
    }

}

