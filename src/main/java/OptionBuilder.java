import java.io.*;
import java.util.Scanner;

public class OptionBuilder {

    private int client_value;
    private final String seen_movies = "seenmovies.txt";
    private final String wish_list_movies = "wishlist.txt";

    OptionBuilder(int value) {
        client_value = value;
    }


    /***  Provide different functions for user's key pressed  ***/

    public void UserOption(Json_Utils JsonObj) {
        switch (client_value) {
            case 1:
                printSeenMovies(JsonObj);
                break;
            case 2:
                printToSeeMovies(JsonObj);
                break;
            case 3:
                  SearchMovieByTitle(JsonObj);
                break;
            case 4:
                  SearchById(JsonObj);
                break;
            case 5:
                 SearchByKeyWord(JsonObj);
                break;
            case 6:
                AddMovie();
                break;
            case 7:
                WishList();
                break;
            case 8:
                 PrintSummary(JsonObj);
                 break;
            default: {
                System.out.println("Wrong value!. Try again: ");
                client_value = Integer.parseInt(GetPressedWord());
                UserOption(JsonObj);
            }
        }
    }

    /***  Get the input from keyboard  ***/
    private String GetPressedWord(){
        Scanner Scan = new Scanner(System.in);
        return Scan.next();
    }

    private void SearchByKeyWord(Json_Utils JObj){
        System.out.println("Introduceti cuvantul cheie al filmului!: ");
        JObj.SearchMovieByKeyWord(GetPressedWord());
    }

    private void SearchById(Json_Utils JObj){
        System.out.println("Introduceti id-ul filmului!: ");
        JObj.PrintMovieDetailsById(GetPressedWord());

    }

    private void SearchMovieByTitle(Json_Utils JsonObj){
        System.out.println("Introduceti Titlul filmului!: ");
        JsonObj.PrintMovieDetailsByTitle(GetPressedWord());
    }

    private  void PrintSummary(Json_Utils JObj){
        System.out.println("Introduceti id-ul filmului!: ");
        JObj.PrintMovieSummary(GetPressedWord());
    }

    /*** write movie  title into file  ***/
    private void writeToSeenMovies(String Title, String fileName) throws IOException {
        FileWriter writer = new FileWriter(fileName,true);
        writer.write(Title);
        writer.write("\n");
        System.out.println("Done!");
        writer.close();
    }

    /***  Read every title from file and print movie with its title or NonValue if not exists  ***/
     void PrintMovieFromFile(Json_Utils JsonObj, String fileName) {
        try {
            System.out.println(fileName);
            File file = new File(fileName);    //creates a new file instance
            FileReader fr = new FileReader(file);   //reads the file
            BufferedReader br = new BufferedReader(fr);  //creates a buffering character input stream
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println("IMDB iD: " + line);
                JsonObj.PrintMovie(line);
            }
            fr.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

     void printToSeeMovies(Json_Utils JsonObj) {
        PrintMovieFromFile(JsonObj, wish_list_movies);
    }

    /***  ***/
     void printSeenMovies(Json_Utils JsonObj) {
        PrintMovieFromFile(JsonObj, seen_movies);
    }

    /***  Read Movie id into wish list file  ***/
    private void WishList() {
        System.out.println("Introduceti Id-ul filmului!: ");
        String Movie_Id = GetPressedWord();
        try {
            writeToSeenMovies(Movie_Id, wish_list_movies);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /***  write movie id into file  ***/
    private void AddMovie() {
        System.out.println("Introduceti Id-ul filmului!: ");
        String Movie_Id = GetPressedWord();
        try {
            writeToSeenMovies(Movie_Id, seen_movies);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
