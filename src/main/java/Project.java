import java.util.Scanner;

public class Project {
    public static void main(String[] args) {

        System.out.println("Alegeti o optiune:\n" +
                "1. Afisare filme vizionate\n" +
                "2. Afisare filme de vizionat\n" +
                "3. Cautare film dupa titlu\n" +
                "4. Cautare film dupa id IMDB\n" +
                "5. Cautare film dupa cuvant cheie\n" +
                "6. Adauga film in lista de vizionate\n" +
                "7. Adauga film in lista de viitoare vizionari\n" +
                "8. Afisare rezumat film\n");


        /*** Get user choice and send it to OptionBuilder object  ***/

        Scanner scan = new Scanner(System.in);
        int scanValue = scan.nextInt();
        System.out.println(scanValue);

        /***  Initialize the classes and get to work***/
        Json_Utils JsonUtil = new Json_Utils();
        OptionBuilder MyBuilder = new OptionBuilder(scanValue);
        MyBuilder.UserOption(JsonUtil);
    }
}